import React from "react";
import Product from "./Product";
import ProductsLoader from "./ProductsLoader";
import Error from "./Error";

class UpdateProduct extends React.Component {
  render() {
    if (this.props.isLoading) {
      return <ProductsLoader />;
    }
    if (this.props.isError) {
      return <Error error={this.state.errorMessage} />;
    }
    if (this.props.products.length > 0) {
      return (
        <div className="product-cards update-cards">
          {this.props.products.map((product, index) => {
            return (
              <div
                className="select-update-card"
                onClick={() => {
                  this.props.selectProduct(index);
                }}
                key={product.id}
              >
                <Product
                  productId={product.id}
                  title={product.title}
                  image={product.image}
                  rating={product.rating}
                  price={product.price}
                  category={product.category}
                  description={product.description}
                />
              </div>
            );
          })}
        </div>
      );
    }
  }
}

export default UpdateProduct;
