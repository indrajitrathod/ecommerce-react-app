import React from "react";
import UpdateForm from "./UpdateForm";
import Popup from "./PopUp";
import UpdateProduct from "./UpdateProduct";
import ProductsLoader from "./ProductsLoader";
import Error from "./Error";
import Footer from "./Footer";
import { Link } from "react-router-dom";

class Updateproducts extends React.Component {
  render() {
    return (
      <>
        <header>
          <div className="header-content">
            <div className="logo">
              <Link to="/">
                MOUNTBLUE
                <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                CART
              </Link>
            </div>
          </div>
        </header>
        <main>
          <div className="main-content">
            <h2>Update Products details here</h2>
            {this.props.isLoading && <ProductsLoader />}
            {!this.props.isLoading &&
              !this.props.isError &&
              this.props.products.length === 0 && <ProductsLoader />}
            {this.props.isError && <Error error={this.props.errorMessage} />}
            {!this.props.isLoading &&
              !this.props.isError &&
              this.props.products.length > 0 && (
                <div className="update-container">
                  <UpdateProduct
                    products={this.props.products}
                    selectProduct={this.props.selectProductToEdit}
                  ></UpdateProduct>
                  {this.props.showWarningPopup && (
                    <Popup
                      message={"warning"}
                      close={this.props.closePopup}
                      discard={this.props.updateChangeProductForm}
                      closeSuccessPopup={this.props.closeSuccessPopup}
                    ></Popup>
                  )}
                  {this.props.showSuccessPopup && (
                    <Popup
                      message={"success"}
                      close={this.props.closePopup}
                      discard={this.props.updateChangeProductForm}
                      closeSuccessPopup={this.props.closeSuccessPopup}
                    ></Popup>
                  )}
                  <UpdateForm
                    product={this.props.product}
                    handleChange={this.props.handleChange}
                    handleFormSubmit={this.props.handleFormSubmit}
                    isChanged={this.props.isChanged}
                    isSuccess={this.props.isSuccess}
                  ></UpdateForm>
                </div>
              )}
          </div>
        </main>
        <Footer />
      </>
    );
  }
}

export default Updateproducts;
