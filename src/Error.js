import React from "react";

class Error extends React.Component {
    render(){
        return(
            <div className="error-container">
                {this.props.error}
            </div>
        );
    }
}

export default Error;