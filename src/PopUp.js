import React from "react";

export class Popup extends React.Component {
  render() {
    return (
      <div className="popup-container">
        <div className="popup">
          <p className="popup-title">
            {this.props.message==='warning' && <>There are unsaved changes. Discard the changes?</>}
            {this.props.message==='success' && <>Successfully Updated Cart Details</>}
          </p>
          {this.props.message==='warning' && (<div className="buttons">
            <button className="popup-button" onClick={this.props.discard}>
              Yes
            </button>
            <button className="popup-button" onClick={this.props.close}>
              No
            </button>
          </div>)}
          {this.props.message==='success' && (<div className="buttons">
            <button className="popup-button" onClick={this.props.closeSuccessPopup}>
              OK. Edit Next Product
            </button>
          </div>)}
        </div>
      </div>
    );
  }
}

export default Popup;
