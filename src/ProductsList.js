import React from "react";
import productsUrl from "./api-config";
import Product from "./Product";
import ProductsLoader from "./ProductsLoader";
import Error from "./Error";
import { Link } from "react-router-dom";

class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      products: [],
      isError: false,
      errorMessage: null,
    };
  }

  componentDidMount() {
    productsUrl
      .get("products")
      .then((response) => {
        this.setState({
          products: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: true,
          errorMessage: `Couldn't fetch Items, try Again after sometime`,
          isLoading: false,
        });
      });
  }

  render() {
    if (this.state.isLoading) {
      return <ProductsLoader />;
    }
    if (this.state.isError) {
      return <Error error={this.state.errorMessage} />;
    }
    if (this.state.products.length > 0) {
      return (
        <div className="product-cards">
          {this.state.products.map((product) => {
            return (
              <Link to={`/ProductDetails/${product.id}`} key={product.id}>
                <Product
                  productId={product.id}
                  title={product.title}
                  image={product.image}
                  rating={product.rating}
                  price={product.price}
                  category={product.category}
                  description={product.description}
                />
              </Link>
            );
          })}
        </div>
      );
    }
  }
}

export default ProductList;
