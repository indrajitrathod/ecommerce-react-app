import React from "react";

class Rating extends React.Component {
  render() {
    const { rate, count } = this.props.rating;
    const productId = this.props.productId;
    const filledStars = new Array(Math.floor(rate))
      .fill(1)
      .map((star, index) => {
        return (
          <i className="fa fa-star" key={`${productId}_${star}_${index}`}></i>
        );
      });
    const emptyStars = new Array(Math.floor(5 - filledStars.length))
      .fill(0)
      .map((star, index) => {
        return (
          <i className="fa fa-star-o" key={`${productId}_${star}_${index}`}></i>
        );
      });
      
    return (
      <React.Fragment>
        {filledStars}{emptyStars}
        <span>{rate}</span>(<span className="count">{count}</span>)
      </React.Fragment>
    );
  }
}

export default Rating;
