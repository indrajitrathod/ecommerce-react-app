import React from "react";
import Footer from "./Footer";
import { Link } from "react-router-dom";

class NotFound extends React.Component {
  render() {
    return (
      <>
        <header>
          <div className="header-content">
            <div className="logo">
              <Link to="/">
                MOUNTBLUE
                <i className="fa fa-shopping-cart" aria-hidden="true"></i>
                CART
              </Link>
            </div>
          </div>
        </header>
        <main>
          <div className="main-content">
            <h1>Page Not Found</h1>
            <Link to="/">
              <h3 style={{ color: "#da6635" }}>
                <i className="fa fa-home" style={{ marginInline: "1rem" }}></i>
                Go to Main Page
              </h3>
            </Link>
          </div>
        </main>
        <Footer />
      </>
    );
  }
}

export default NotFound;
