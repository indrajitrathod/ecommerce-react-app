import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import CartList from "./CartList";

class Cart extends React.Component {

  render() {
    return (
      <>
        <Header productsInCart={this.props.productsInCart} />
        <main>
          <div className="main-content">
            <CartList
              productsInCart={this.props.productsInCart}
              manageProductsInCart={this.props.manageProductsInCart}
            />
          </div>
        </main>
        <Footer />
      </>
    );
  }
}

export default Cart;
