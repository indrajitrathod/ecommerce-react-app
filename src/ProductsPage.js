import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import ProductList from "./ProductsList";

class ProductsPage extends React.Component {
  render() {
    return (
      <>
        <Header productsInCart={this.props.productsInCart} />
        <main>
          <div className="main-content">
            <section className="products">
              <h2>Our Products</h2>
              <ProductList />
            </section>
          </div>
        </main>
        <Footer />
      </>
    );
  }
}

export default ProductsPage;
