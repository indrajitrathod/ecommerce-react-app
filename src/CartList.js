import React from "react";

class CartList extends React.Component {
  render() {
    const productsInCart = this.props.productsInCart;
    const totalPrice = productsInCart
      .reduce((sum, product) => {
        return (sum += product.price);
      }, 0)
      .toFixed(2);
    return (
      <>
        <div className="cart-container">
          <ul>
            {productsInCart.length > 0 && <h3 style={{'text-align': 'center', color: '#da6635'}}>Your Cart here.!</h3>}
            {productsInCart.length > 0 &&
              productsInCart.map((product) => {
                return (
                  <li key={product.id}>
                    <div className="image-container">
                      <img src={product.image} alt={product.title} />
                    </div>
                    <div className="cart-item-details">
                      <h3>{product.title}</h3>
                      <div className="category">
                        Category: <span>{product.category}</span>
                      </div>
                      <div className="price">
                        Price: <span>${product.price.toFixed(2)}</span>
                      </div>

                      <div className="quantity">
                        Count:
                        <button
                          className="quantity__minus"
                          onClick={() => {
                            const presentItem = { ...product };
                            const presentItemIndex = productsInCart.findIndex(
                              (selectedProduct) => {
                                return selectedProduct.id === product.id;
                              }
                            );
                            const otherItems = productsInCart.filter(
                              (selectedProduct) => {
                                return selectedProduct.id !== product.id;
                              }
                            );
                            presentItem.price -=
                              presentItem.price / presentItem.count;
                            presentItem.count -= 1;
                            const updatedCart = [...productsInCart];
                            updatedCart[presentItemIndex] = presentItem;
                            presentItem.count === 0
                              ? this.props.manageProductsInCart(otherItems)
                              : this.props.manageProductsInCart(updatedCart);
                          }}
                        >
                          <span>-</span>
                        </button>
                        <span className="product-count">{product.count}</span>
                        <button
                          className="quantity__plus"
                          onClick={() => {
                            const presentItem = { ...product };
                            const presentItemIndex = productsInCart.findIndex(
                              (selectedProduct) => {
                                return selectedProduct.id === product.id;
                              }
                            );
                            presentItem.price +=
                              presentItem.price / presentItem.count;
                            presentItem.count += 1;
                            const updatedCart = [...productsInCart];
                            updatedCart[presentItemIndex] = presentItem;
                            this.props.manageProductsInCart(updatedCart);
                          }}
                        >
                          <span>+</span>
                        </button>
                      </div>
                      <button
                        className="remove-item"
                        onClick={() => {
                          const updatedCart = productsInCart.filter(
                            (selectedProduct) => {
                              return product.id !== selectedProduct.id;
                            }
                          );
                          this.props.manageProductsInCart(updatedCart);
                        }}
                      >
                        <i className="fa fa-trash"></i>
                        Remove Item
                      </button>
                    </div>
                  </li>
                );
              })}
            {productsInCart.length > 0 && (
              <li className="total-price">
                Total Price: &nbsp;&nbsp;{" "}
                <span className="price"> {`$${totalPrice}`}</span>
              </li>
            )}
            {productsInCart.length === 0 && <div>No Products In Cart</div>}
          </ul>
        </div>
      </>
    );
  }
}

export default CartList;
