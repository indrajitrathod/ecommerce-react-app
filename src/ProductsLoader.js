import React from "react";

class ProductsLoader extends React.Component{
    render(){
        return(
            <div className="products-loading">
              <div className="loader"></div>
            </div>
        );
    }
}

export default ProductsLoader;