import React from "react";
import { Link } from "react-router-dom";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartItemsCount: 7,
    };
  }
  render() {
    const numberOfProductsInCart = this.props.productsInCart.reduce(
      (totalProducts, product) => {
        return (totalProducts += product.count);
      },
      0
    );

    const cartCount = {
      "--cart-items-count": `'${numberOfProductsInCart.toString()}'`,
    };

    return (
      <header>
        <div className="header-content">
          <div className="logo">
            <Link to="/">
              MOUNTBLUE
              <i className="fa fa-shopping-cart" aria-hidden="true"></i>
              CART
            </Link>
          </div>
          <Link to="/updateproducts">Update Product Details</Link>
          <Link to="/cart">
            <i
              className="fa fa-shopping-cart header-cart"
              style={cartCount}
            ></i>
          </Link>
        </div>
      </header>
    );
  }
}

export default Header;
