import React from "react";
import productsUrl from "./api-config";
import ProductsLoader from "./ProductsLoader";
import Error from "./Error";
import SingleProduct from "./SingleProduct";
import Header from "./Header";
import Footer from "./Footer";
import { withRouter } from "react-router-dom";

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      productDetails: null,
      isError: false,
      errorMessage: null,
    };
    this.productId = this.props.match.params.id;
  }

  componentDidMount() {
    productsUrl
      .get(`/products/${this.productId}`)
      .then((response) => {
        this.setState({
          productDetails: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: true,
          errorMessage: `Couldn't fetch Items, try Again after sometime`,
          isLoading: false,
        });
      });
  }

  render() {
    return (
      <React.Fragment>
        <Header productsInCart={this.props.productsInCart}/>
        <main>
          <div className="main-content">
            {this.state.isLoading && <ProductsLoader />}
            {this.state.isError && <Error error={this.state.errorMessage} />}
            {!this.state.isLoading && !this.state.isError && (
              <SingleProduct
                productId={this.state.productDetails.id}
                title={this.state.productDetails.title}
                image={this.state.productDetails.image}
                rating={this.state.productDetails.rating}
                price={this.state.productDetails.price}
                category={this.state.productDetails.category}
                description={this.state.productDetails.description}
                productsInCart={this.props.productsInCart}
                manageProductsInCart={this.props.manageProductsInCart}
              />
            )}
          </div>
        </main>

        <Footer />
      </React.Fragment>
    );
  }
}

export default withRouter(ProductDetails);
