import React from "react";
import ProductsPage from "./ProductsPage";
import ProductDetails from "./ProductDetails";
import productsUrl from "./api-config";
import NotFound from "./NotFound";
import Cart from "./Cart";
import Updateproducts from "./UpdateProducts";
import { Route, Switch } from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cartItems: [],
      isLoading: true,
      products: [],
      isError: false,
      errorMessage: null,
      productToEdit: null,
      isChanged: false,
      successfullyUpdated: false,
      showWarningPopup: false,
      showSuccessPopup: false,
      currentProductIndex: null,
    };
  }

  handleProductsInCart = (products) => {
    this.setState(() => {
      return { cartItems: products };
    });
  };

  selectProductToEdit = (index) => {
    if (!this.state.isChanged) {
      this.setState((state) => ({
        productToEdit: state.products[index],
        currentProductIndex: index,
        successfullyUpdated: false,
      }));
    } else {
      this.setState({
        showWarningPopup: true,
        tempIndex: index,
      });
    }
  };

  updateChangeProductForm = () => {
    this.setState(
      {
        isChanged: false,
        showWarningPopup: false,
      },
      () => {
        this.selectProductToEdit(this.state.tempIndex);
      }
    );
  };

  handleFormSubmit = () => {
    this.setState((state) => {
      let products = state.products;
      products[state.currentProductIndex] = state.productToEdit;
      return {
        products,
        successfullyUpdated: true,
        isChanged: false,
        productToEdit: null,
        showSuccessPopup: true,
      };
    });
  };

  handleChange = (event) => {
    const name = event.target.name;
    this.setState((state) => ({
      isChanged: true,
      productToEdit: { ...state.productToEdit, [name]: event.target.value },
    }));
  };

  closePopup = () => {
    this.setState({
      showWarningPopup: false,
    });
  };

  closeSuccessPopup = () => {
    this.setState({
      showSuccessPopup: false,
    });
  };

  componentDidMount() {
    productsUrl
      .get("products")
      .then((response) => {
        this.setState({
          products: response.data,
          isLoading: false,
        });
      })
      .catch(() => {
        this.setState({
          isError: true,
          errorMessage: `Couldn't fetch Items, try Again after sometime`,
          isLoading: false,
        });
      });
  }

  render() {
    return (
      <Switch>
        <Route exact path="/">
          <ProductsPage productsInCart={this.state.cartItems} />
        </Route>
        <Route exact path="/productdetails/:id">
          <ProductDetails
            productsInCart={this.state.cartItems}
            manageProductsInCart={this.handleProductsInCart}
          />
        </Route>
        <Route exact path="/updateproducts">
          <Updateproducts
            products={this.state.products}
            selectProductToEdit={this.selectProductToEdit}
            product={this.state.productToEdit}
            handleChange={this.handleChange}
            handleFormSubmit={this.handleFormSubmit}
            isChanged={this.state.isChanged}
            isSuccess={this.state.successfullyUpdated}
            closePopup={this.closePopup}
            updateChangeProductForm={this.updateChangeProductForm}
            showWarningPopup={this.state.showWarningPopup}
            showSuccessPopup={this.state.showSuccessPopup}
            closeSuccessPopup={this.closeSuccessPopup}
          />
        </Route>
        <Route exact path="/cart">
          <Cart
            productsInCart={this.state.cartItems}
            manageProductsInCart={this.handleProductsInCart}
          />
        </Route>
        <Route>
          <NotFound />
        </Route>
      </Switch>
    );
  }
}

export default App;
