import React from "react";
import Rating from "./Rating";

class SingleProduct extends React.Component {
  addTocart = () => {
    const isThisProductInCart = this.props.productsInCart.findIndex(
      (product) => {
        return product.id === this.props.productId;
      }
    );

    if (isThisProductInCart >= 0) {
      const cartProducts = this.props.productsInCart.filter((product) => {
        return product.id !== this.props.productId;
      });
      this.props.manageProductsInCart(cartProducts);
    } else {
      const thisProduct = {
        id: this.props.productId,
        title: this.props.title,
        image: this.props.image,
        category: this.props.category,
        price: this.props.price,
        count: 1,
      };
      const cartProducts = [...this.props.productsInCart, thisProduct];
      this.props.manageProductsInCart(cartProducts);
    }
  };

  render() {
    const {
      productId,
      title,
      image,
      price,
      rating,
      description,
      productsInCart,
    } = this.props;

    const isThisProductInCart = productsInCart.findIndex((product) => {
      return product.id === productId;
    });

    return (
      <div className="product-card-single">
        <div className="product-image">
          <img src={image} alt="" />
        </div>
        <div className="product-details">
          <h3>{title}</h3>
          <Rating rating={rating} productId={productId} />
          <h4>
            <small>$</small>
            <span className="price">{price.toFixed(2)}</span>
          </h4>
          <p>{description}</p>
          <button onClick={this.addTocart}>
            {!(isThisProductInCart >= 0) && (
              <span>
                <i className="fa-solid fa-cart-plus"></i>Add To Cart
              </span>
            )}
            {isThisProductInCart >= 0 && (
              <span>
                <i className="fa fa-check-circle"></i>
                Added To Cart
              </span>
            )}
          </button>
        </div>
      </div>
    );
  }
}

export default SingleProduct;
