import axios from "axios";

const productsUrl = axios.create({
  baseURL: "https://fakestoreapi.com",
});

export default productsUrl;
