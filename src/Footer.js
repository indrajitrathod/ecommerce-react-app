import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer>
        <div className="footer-content">
          <div className="important-links">
            <ul>
              <h4>About</h4>
              <li>Contact Us</li>
              <li>About Us</li>
              <li>Careers</li>
            </ul>
            <ul>
              <h4>Help</h4>
              <li>Payments</li>
              <li>Cancellations & Returns</li>
              <li>FAQs</li>
            </ul>
            <ul>
              <h4>Policy</h4>
              <li>Return Policy</li>
              <li>Terms of Use</li>
              <li>Privacy</li>
            </ul>
          </div>
          <div className="social-media">
            <ul>
              <h4>Social Media</h4>
              <li>Facebook</li>
              <li>Twitter</li>
              <li>YouTube</li>
            </ul>
          </div>
        </div>
        <hr />
        <div className="copyright">&#169;2022 mountbluecart.com</div>
      </footer>
    );
  }
}

export default Footer;
